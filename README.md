# README #

storjrb-backupcore is a generic Ruby script that facilitates the automation of multiple Storj backups.  

The best parts about it:

* Only requires Ruby and ```npm install storj```
* Easily bolts on to any existing backup solution.
* Configured with YAML.
* So incredibly generic that you can probably clone it in whatever scripting language you're most comfortable with.
* Potentially much easier to use than manual configuration for novice users.

The worst parts about it:

* Has a **terrible** name.
* Doesn't do threading yet, so you might run into problems if you're backing up huge numbers of directories.

### How do I get set up? ###

0. Make sure you have Ruby and Storj installed.
1. Copy the script and .coreConfig.yaml into a directory.
2. Add a logfile, ie. ```touch myAmazingLogfile.log```.
3. Fill in the .coreConfig.yaml file to your liking (probably also want to add that to your .gitignore if you're putting it in a tracked repository).  I've provided comments on what each config line does- but if anything's not clear, let me know.
4. Add the fully qualified path to your .coreConfig.yaml file to backupCore.rb on line 11 (this will be abstracted out in the next release)
5. Add the fully qualified path to storj on line 85 of backupCore.rb (this will also be abstracted out in the next release)
6. Set up a cron/rufus/foreman/etc job to call backupCore.rb.


```
#Example crontab

#>crontab -e
#note that you have to use the fully qualified path to ruby/whatever language you're using for cron to trigger it right.
0 21 * * 5 /usr/bin/ruby /home/indy/automation/backupCore.rb
```

7. Sit back and relax.

That's pretty much it.  

I'm also currently thinking about a basic Storj wrapper for Ruby, which will eventually be incorporated into this project, but any feature requests/input are welcome!

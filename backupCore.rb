require 'yaml'
require 'open3'                   #http://blog.honeybadger.io/capturing-stdout-stderr-from-shell-commands-via-ruby/
require 'logger'


#Get @date
@date = Time.now.strftime("%m%d%y")

#This is currently hardcoded and I will probably move it to an ARGV in the next release.
#	It has to be fully qualified to play nicely with cron.
@opt_hash = YAML.load(File.read("/FULLY/QUALIFIED/PATH/TO/config.yaml"))

@storj_key = @opt_hash["storj_key"]
@logfile = @opt_hash["logfile"]

#Create a logger which ages the logfile once it reaches a certain size. Leave 10 “old” log files where each file is about 1,024,000 b$
LOGGER = Logger.new(@logfile,10,1024000)


#--------Mapping from .coreConfig.yml for reference--------#
#
#.coreConfig.yml is parsed into a hash that looks like this:
#
#{
#   "storj_key"=>"your-secret-key", 
#   "job_list"=>
#   {
#       "example_moniker"=>
#       {
#           "storj_bucket"=>"3X8MPL3BUCK371D", 
#           "input_dir"=>"/your/backups/", 
#           "staging_dir"=>"/your/staging/", 
#           "output_prefix"=>"prefix", 
#           "output_suffix"=>"suffix"
#       }
#   }
#}

def main
  queue = []
  @opt_hash["job_list"].each do |key,value|
    queue.push(UploadJob.new(key,value))
  end

  queue.each do |j|
    j.stage(@date)
    j.perform(@storj_key,@date)
    j.cleanup
  end

end

class UploadJob
  def initialize(moniker,opt)
    @moniker = moniker
    opt.each do |key,value|
      instance_variable_set("@#{key}", value)
    end
  end

  #debugging
  def show
    puts @moniker
    puts @storj_bucket
    puts @input_dir
    puts @staging_dir
    puts @output_prefix
    puts @output_suffix
  end

  def stage(date)
    LOGGER.info("Running #{@moniker}.stage(#{date})")
    stdout, stderr = Open3.capture3("tar czvhf #{@staging_dir}/#{@output_prefix}-#{date}-#{@output_suffix}.tar.gz #{@input_dir}")
    LOGGER.info(stdout)
    LOGGER.error(stderr)
  end

  def perform(key,date)
    #This is also hardcoded at the moment and will probably be moved to the config file in the next release.
    #	To find the absolute path to storj, just run
    #
    #	>which storj
    #
    #	and paste the output inside of the quotation marks.
    storj_path = "/home/user/.nvm/versions/node/v7.2.0/bin/storj"
    LOGGER.info("Running #{@moniker}.perform(key,#{date})")
    stdout, stderr = Open3.capture3("#{storj_path} -k '#{key}' upload-file #{@storj_bucket} #{@staging_dir}/#{@output_prefix}-#{date}$

    #Raw command debugging in case you're having trouble getting cron to run.
    #command = "#{storj_path} -k '#{key}' upload-file #{@storj_bucket} #{@staging_dir}/#{@output_prefix}-#{date}-#{@output_suffix}.ta$
    #puts command
    #exec(command)

    LOGGER.info(stdout)
    LOGGER.error(stderr)
  end

  def cleanup
    LOGGER.info("Running #{@moniker}.cleanup")
    #Clears staging directory
    stdout, stderr = Open3.capture3("rm -rf #{@staging_dir}/*")
    LOGGER.info(stdout)
    LOGGER.error(stderr)
  end

end

main
#smoke

